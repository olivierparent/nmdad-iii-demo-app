(function () { 'use strict';

    angular.module('artevelde-directives')
        .directive('arteveldeHelloAgain', function () {
            return {
                restrict: 'EACM', // Voor gebruik op Element, Attribute, Class en Comment
                replace: true,
                templateUrl: 'templates/directives/arteveldeHelloAgain.html'
            };
        })
    ;

})();
