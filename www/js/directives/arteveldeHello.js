(function () { 'use strict';

    angular.module('artevelde-directives')
        .directive('arteveldeHello', ['$log', function ($log) {

            var i = 0;

            return {
                restrict: 'EACM', // Voor gebruik op Element, Attribute, Class en Comment
                link: function () {

                    $log.info('Hello from Directive arteveldeHello to MMP proDEV (' + ++i + ')!')
                }
            };
        }])
    ;

})();
