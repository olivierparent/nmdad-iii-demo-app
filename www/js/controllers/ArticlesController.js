/**
 * ArticlesController
 */
(function () { 'use strict';

    angular.module('artevelde-controllers')
        .controller('artevelde-controllers-ArticlesController', ['$rootScope', '$scope', '$filter', '$http', '$location', '$log', '$state', '$ionicLoading', 'ArticlesService', function ($rootScope, $scope, $filter, $http, $location, $log, $state, $ionicLoading, ArticlesService) {

            // Controller Properties

            $rootScope.articles = $rootScope.articles || [];

            $scope.article = {
                id: null,
                title: null,
                body: null
            };

            $rootScope.user = {
                id: 1
            };

            // Controller Actions

            $scope.getAllAction = function () {
                $ionicLoading.show({
                    template: 'Loading...'
                });

                ArticlesService
                    .getAll()
                    .then(function (result) {
                        $ionicLoading.hide();
                        $rootScope.articles = result.data;
                        $log.info(result.data);
                    })
                ;
            };

            $scope.getAction = function () {

                ArticlesService
                    .get($scope.article)
                    .success(function (data, status, headers, config) {
                        $scope.article.id    = data.id;
                        $scope.article.title = data.title;
                        $scope.article.body  = data.body;
                    })
                    .error(function (data, status, headers, config) {

                    })
                ;

            }

            $scope.postAction = function () {

                $ionicLoading.show({
                    template: 'Saving...'
                });

                // Add article to list.
                var count = $rootScope.articles.push($scope.article);

                ArticlesService
                    .post($scope.article)
                    .success(function (data, status, headers, config) {
                        var location = headers('location'); // 'Location' header must be exposed.
                        var id = location.substr(location.lastIndexOf('/') + 1);
                        $rootScope.articles[count - 1].id = id;
                        $ionicLoading.hide();
                        $log.info("Article saved");
                    })
                    .error(function (data, status, headers, config) {
                        // Remove previously added article
                        $rootScope.articles.pop();
                        $ionicLoading.hide();
                        $log.error("Article not saved");
                    })

                ;
            };

            $scope.putAction = function () {

                $ionicLoading.show({
                    template: 'Saving...'
                });

                $log.info($scope.article);

                ArticlesService
                    .put($scope.article)
                    .success(function (data, status, headers, config) {
                        $ionicLoading.hide();
                        $log.info("Article saved");
                    })
                    .error(function (data, status, headers, config) {
                        // Remove previously added article
                        $ionicLoading.hide();
                        $log.error("Article not saved");
                    })

                $log.info('putAction');
            };

            $scope.deleteAction = function (index) {

                $scope.article = $rootScope.articles[index];

                ArticlesService
                    .del($scope.article);
                $log.info('deleteAction: ' + $scope.article.id);
            };

            $scope.editAction = function (index) {
                $location.path('/articles/' + $scope.articles[index].id +'/edit');
            };

            // Actions

            if ($state.is('articles-list') && $rootScope.articles.length <= 0) {
                $scope.getAllAction();
            }

            if ($state.is('articles-edit')) {
                $scope.article.id = $state.params.articleId;
                $scope.getAction();
            }

        }])
    ;

})();
