/**
 * DefaultController
 */
(function () { 'use strict';

    angular.module('artevelde-controllers')
        .controller('artevelde-controllers-DefaultController', ['$scope', function ($scope) {

            $scope.title   = "New Media Design & Development III";
            $scope.message = "Hello MMP proDEV!";

        }])
    ;

})();
