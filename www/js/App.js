(function () { 'use strict';

    // Setter voor App-module, met dependencies ()
    var App = angular.module('artevelde', [
        'ionic',
        'artevelde-controllers',
        'artevelde-directives',
        'artevelde-filters',
        'artevelde-services'
    ]);

    // Setters voor overige modules (let op de lege array voor de dependencies!)
    angular.module('artevelde-controllers', []);
    angular.module('artevelde-directives' , []);
    angular.module('artevelde-filters'    , []);
    angular.module('artevelde-services'   , []);

    // App-module
    App
        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {
                // @see https://github.com/driftyco/ionic-plugins-keyboard
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })
        .config(['$compileProvider', '$httpProvider', function ($compileProvider, $httpProvider) {
            // Allow 'app:' as protocol (for use in Hybrid Mobile apps)
            $compileProvider
                .aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|app):/)
                .imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|app):|data:image\/)/)
            ;

            // Enable CORS (Cross-Origin Resource Sharing)
            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
        }])
        .constant('apiConfig', {
            //domain: 'http://dev.nmdad-iii.arteveldehogeschool.be',
            domain: 'http://dev.nmdad3.arteveldehogeschool.local',
            path: '/api/v1/'
        })
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'templates/views/home.html'
                })
                .state('articles-add', {
                    url: '/articles/add',
                    templateUrl: 'templates/views/articles/add.html'
                })
                .state('articles-edit', {
                    url: '/articles/:articleId/edit',
                    templateUrl: 'templates/views/articles/edit.html'
                })
                .state('articles-list', {
                    url: '/articles',
                    templateUrl: 'templates/views/articles/list.html'
                })
                .state('directives-demo', {
                    url: '/directives-demo',
                    templateUrl: 'templates/views/directives-demo.html'
                })
            ;
            $urlRouterProvider.otherwise('/');
        }])
    ;

})();
