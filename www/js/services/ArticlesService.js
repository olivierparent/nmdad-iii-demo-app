/**
 * ArticlesService
 */
(function () { 'use strict';

    var Service = function ($rootScope, $filter, $http, apiConfig) {
        var uri  = apiConfig.domain + apiConfig.path;

        this.getAll = function () {
            // JSON_CALLBACK is een AngularJS-variabele die op het ogenblik van de request vervangen wordt.
            return $http
                .jsonp(uri + 'articles.jsonp?callback=JSON_CALLBACK');
        };

        this.get = function (article) {

            return $http
                .get(uri +'users/' + $rootScope.user.id + '/articles/' + article.id);

        };

        this.post = function (article) {

            delete article.id;

            var data = $filter('json')({ article: article });

            return $http
                .post(uri +'users/' + $rootScope.user.id + '/articles/', data);

        };

        this.put = function (article) {

            var data = $filter('json')({ article: article });

            return $http
                .put(uri +'users/' + $rootScope.user.id + '/articles/' + article.id, data);

        };

        this.del = function (article) {
            return $http
                .delete(uri +'users/' + $rootScope.user.id + '/articles/' + article.id);

        };
    };

    angular.module('artevelde-services')
        .service('ArticlesService', Service)
    ;

})();
