/**
 * xFilter
 */
(function () { 'use strict';

    angular.module('artevelde-filters')
        .filter('appendTime', function () {
            return function (input) {
                var d = new Date();

                return input + ' ' + d.toDateString();
            };
        })
    ;

})();
