New Media Design & Development III
==================================

I. Benodigdheden
----------------

 - [Apache Cordova](http://cordova.apache.org)
 - [Ionic Framework](http://ionicframework.com) 
 - [NMDAD III Demo](https://bitbucket.org/olivierparent/nmdad-iii-demo)

II. Installatie
---------------

    $ cd ~/Code/nmdad-iii.arteveldehogeschool.be/app/
    $ ionic platform firefoxos
    
III. Uitvoeren
--------------

### 1. Firefox Developer Edition

    $ ionic serve
    
En open de [app](http://localhost:8100/) in Responsive Design-weergave.

### 2. Firefox OS Simulator van Firefox Developer Edition

    $ cordova prepare firefoxos 
  
En open in WebIDE de verpakte app (`platforms/firefoxos/www/`), selecteer een Runtime (bijv. Firefox OS 2.2 Simulator) en klik op **Installeer en uitvoeren**.
